(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["application"],{

/***/ "./app/javascript/components/Loadable.js":
/*!***********************************************!*\
  !*** ./app/javascript/components/Loadable.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Loadable; });
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.keys */ "./node_modules/core-js/modules/es.object.keys.js");
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);





var _this = undefined,
    _jsxFileName = "/opt/demarches-simplifiees/app/javascript/components/Loadable.js";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var Loader = function Loader() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement("div", {
    className: "spinner left",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4,
      columnNumber: 22
    }
  });
};

function LazyLoad(_ref) {
  var Component = _ref.component,
      props = _objectWithoutProperties(_ref, ["component"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_4__["Suspense"], {
    fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(Loader, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 25
      }
    }),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(Component, Object.assign({}, props, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }
  })));
}

LazyLoad.propTypes = {
  component: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object
};
function Loadable(loader) {
  var LazyComponent = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_4__["lazy"])(loader);
  return function PureComponent(props) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(LazyLoad, Object.assign({
      component: LazyComponent
    }, props, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 12
      }
    }));
  };
}

/***/ }),

/***/ "./app/javascript/loaders sync recursive ^\\.\\/.*$":
/*!**********************************************!*\
  !*** ./app/javascript/loaders sync ^\.\/.*$ ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./Chartkick": "./app/javascript/loaders/Chartkick.js",
	"./Chartkick.js": "./app/javascript/loaders/Chartkick.js",
	"./ComboAdresseSearch": "./app/javascript/loaders/ComboAdresseSearch.js",
	"./ComboAdresseSearch.js": "./app/javascript/loaders/ComboAdresseSearch.js",
	"./ComboCommunesSearch": "./app/javascript/loaders/ComboCommunesSearch.js",
	"./ComboCommunesSearch.js": "./app/javascript/loaders/ComboCommunesSearch.js",
	"./ComboDepartementsSearch": "./app/javascript/loaders/ComboDepartementsSearch.js",
	"./ComboDepartementsSearch.js": "./app/javascript/loaders/ComboDepartementsSearch.js",
	"./ComboRegionsSearch": "./app/javascript/loaders/ComboRegionsSearch.js",
	"./ComboRegionsSearch.js": "./app/javascript/loaders/ComboRegionsSearch.js",
	"./MapEditor": "./app/javascript/loaders/MapEditor.js",
	"./MapEditor.js": "./app/javascript/loaders/MapEditor.js",
	"./MapReader": "./app/javascript/loaders/MapReader.js",
	"./MapReader.js": "./app/javascript/loaders/MapReader.js",
	"./Trix": "./app/javascript/loaders/Trix.js",
	"./Trix.js": "./app/javascript/loaders/Trix.js",
	"./TypesDeChampEditor": "./app/javascript/loaders/TypesDeChampEditor.js",
	"./TypesDeChampEditor.js": "./app/javascript/loaders/TypesDeChampEditor.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./app/javascript/loaders sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./app/javascript/loaders/Chartkick.js":
/*!*********************************************!*\
  !*** ./app/javascript/loaders/Chartkick.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(16), __webpack_require__.e(17)]).then(__webpack_require__.bind(null, /*! ../components/Chartkick */ "./app/javascript/components/Chartkick.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/ComboAdresseSearch.js":
/*!******************************************************!*\
  !*** ./app/javascript/loaders/ComboAdresseSearch.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(18)]).then(__webpack_require__.bind(null, /*! ../components/ComboAdresseSearch */ "./app/javascript/components/ComboAdresseSearch.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/ComboCommunesSearch.js":
/*!*******************************************************!*\
  !*** ./app/javascript/loaders/ComboCommunesSearch.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(19)]).then(__webpack_require__.bind(null, /*! ../components/ComboCommunesSearch */ "./app/javascript/components/ComboCommunesSearch.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/ComboDepartementsSearch.js":
/*!***********************************************************!*\
  !*** ./app/javascript/loaders/ComboDepartementsSearch.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(14), __webpack_require__.e(1), __webpack_require__.e(20)]).then(__webpack_require__.bind(null, /*! ../components/ComboDepartementsSearch */ "./app/javascript/components/ComboDepartementsSearch.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/ComboRegionsSearch.js":
/*!******************************************************!*\
  !*** ./app/javascript/loaders/ComboRegionsSearch.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(21)]).then(__webpack_require__.bind(null, /*! ../components/ComboRegionsSearch */ "./app/javascript/components/ComboRegionsSearch.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/MapEditor.js":
/*!*********************************************!*\
  !*** ./app/javascript/loaders/MapEditor.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(3), __webpack_require__.e(7), __webpack_require__.e(1), __webpack_require__.e(5), __webpack_require__.e(15)]).then(__webpack_require__.bind(null, /*! ../components/MapEditor */ "./app/javascript/components/MapEditor/index.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/MapReader.js":
/*!*********************************************!*\
  !*** ./app/javascript/loaders/MapReader.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(5), __webpack_require__.e(13)]).then(__webpack_require__.bind(null, /*! ../components/MapReader */ "./app/javascript/components/MapReader/index.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/Trix.js":
/*!****************************************!*\
  !*** ./app/javascript/loaders/Trix.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(6), __webpack_require__.e(12), __webpack_require__.e(22)]).then(__webpack_require__.bind(null, /*! ../components/Trix */ "./app/javascript/components/Trix.js"));
}));

/***/ }),

/***/ "./app/javascript/loaders/TypesDeChampEditor.js":
/*!******************************************************!*\
  !*** ./app/javascript/loaders/TypesDeChampEditor.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Loadable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Loadable */ "./app/javascript/components/Loadable.js");



/* harmony default export */ __webpack_exports__["default"] = (Object(_components_Loadable__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(6), __webpack_require__.e(9), __webpack_require__.e(11)]).then(__webpack_require__.bind(null, /*! ../components/TypesDeChampEditor */ "./app/javascript/components/TypesDeChampEditor/index.js"));
}));

/***/ }),

/***/ "./app/javascript/new_design/avis.js":
/*!*******************************************!*\
  !*** ./app/javascript/new_design/avis.js ***!
  \*******************************************/
/*! exports provided: toggleCondidentielExplanation, replaceSemicolonByComma */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggleCondidentielExplanation", function() { return toggleCondidentielExplanation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "replaceSemicolonByComma", function() { return replaceSemicolonByComma; });
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");



function toggleCondidentielExplanation() {
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toggle"])(document.querySelector('.confidentiel-explanation'));
}
function replaceSemicolonByComma(event) {
  event.target.value = event.target.value.replace(/;/g, ',');
}

/***/ }),

/***/ "./app/javascript/new_design/champs/carte.js":
/*!***************************************************!*\
  !*** ./app/javascript/new_design/champs/carte.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.map */ "./node_modules/core-js/modules/es.map.js");
/* harmony import */ var core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_trim__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.trim */ "./node_modules/core-js/modules/es.string.trim.js");
/* harmony import */ var core_js_modules_es_string_trim__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_trim__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");














function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


var inputHandlers = new Map();
addEventListener('ds:page:update', function () {
  var inputs = document.querySelectorAll('.areas input[data-geo-area]');

  var _iterator = _createForOfIteratorHelper(inputs),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var input = _step.value;
      input.addEventListener('focus', function (event) {
        var id = parseInt(event.target.dataset.geoArea);
        Object(_utils__WEBPACK_IMPORTED_MODULE_13__["fire"])(document, 'map:feature:focus', {
          id: id
        });
      });
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
});
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('click', '.areas a[data-geo-area]', function (event) {
  event.preventDefault();
  var id = parseInt(event.target.dataset.geoArea);
  Object(_utils__WEBPACK_IMPORTED_MODULE_13__["fire"])(document, 'map:feature:focus', {
    id: id
  });
});
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('input', '.areas input[data-geo-area]', function (event) {
  var id = parseInt(event.target.dataset.geoArea);
  var handler = inputHandlers.get(id);

  if (!handler) {
    handler = Object(_utils__WEBPACK_IMPORTED_MODULE_13__["debounce"])(function () {
      var input = document.querySelector("input[data-geo-area=\"".concat(id, "\"]"));

      if (input) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_13__["fire"])(document, 'map:feature:update', {
          id: id,
          properties: {
            description: input.value.trim()
          }
        });
      }
    }, 200);
    inputHandlers.set(id, handler);
  }

  handler();
});

/***/ }),

/***/ "./app/javascript/new_design/champs/linked-drop-down-list.js":
/*!*******************************************************************!*\
  !*** ./app/javascript/new_design/champs/linked-drop-down-list.js ***!
  \*******************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");












function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


var PRIMARY_SELECTOR = 'select[data-secondary-options]';
var SECONDARY_SELECTOR = 'select[data-secondary]';
var CHAMP_SELECTOR = '.editable-champ';
Object(_utils__WEBPACK_IMPORTED_MODULE_11__["delegate"])('change', PRIMARY_SELECTOR, function (evt) {
  var primary = evt.target;
  var secondary = primary.closest(CHAMP_SELECTOR).querySelector(SECONDARY_SELECTOR);
  var options = JSON.parse(primary.dataset.secondaryOptions);
  selectOptions(secondary, options[primary.value]);
});

function selectOptions(selectElement, options) {
  selectElement.innerHTML = '';

  var _iterator = _createForOfIteratorHelper(options),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var option = _step.value;
      var element = document.createElement('option');
      element.textContent = option;
      element.value = option;
      selectElement.appendChild(element);
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  selectElement.selectedIndex = 0;
}

/***/ }),

/***/ "./app/javascript/new_design/champs/repetition.js":
/*!********************************************************!*\
  !*** ./app/javascript/new_design/champs/repetition.js ***!
  \********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");












function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


var CHAMP_SELECTOR = '.editable-champ';
var BUTTON_SELECTOR = '.button.remove-row';
var DESTROY_INPUT_SELECTOR = 'input[type=hidden][name*=_destroy]';
var DOM_ID_INPUT_SELECTOR = 'input[type=hidden][name*=deleted_row_dom_ids]';
Object(_utils__WEBPACK_IMPORTED_MODULE_11__["delegate"])('click', BUTTON_SELECTOR, function (evt) {
  evt.preventDefault();
  var row = evt.target.closest('.row');

  var _iterator = _createForOfIteratorHelper(row.querySelectorAll(DESTROY_INPUT_SELECTOR)),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var input = _step.value;
      input.disabled = false;
      input.value = true;
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  row.querySelector(DOM_ID_INPUT_SELECTOR).disabled = false;

  var _iterator2 = _createForOfIteratorHelper(row.querySelectorAll(CHAMP_SELECTOR)),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var champ = _step2.value;
      champ.remove();
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  evt.target.remove();
  row.classList.remove('row'); // We could debounce the autosave request, so that row removal would be batched
  // with the next changes.
  // However *adding* a new repetition row isn't debounced (changes are immediately
  // effective server-side).
  // So, to avoid ordering issues, enqueue an autosave request as soon as the row
  // is removed.

  Object(_utils__WEBPACK_IMPORTED_MODULE_11__["fire"])(row, 'autosave:trigger');
});

/***/ }),

/***/ "./app/javascript/new_design/chartkick.js":
/*!************************************************!*\
  !*** ./app/javascript/new_design/chartkick.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.set-prototype-of */ "./node_modules/core-js/modules/es.object.set-prototype-of.js");
/* harmony import */ var core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.reflect.construct */ "./node_modules/core-js/modules/es.reflect.construct.js");
/* harmony import */ var core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.set */ "./node_modules/core-js/modules/es.set.js");
/* harmony import */ var core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13__);















function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Ruby chartkick helper implementation assumes Chartkick is already loaded.
// It has no way to delay execution. So we wrap all the Chartkick classes
// to queue rendering for when Chartkick is loaded.
var AreaChart = function AreaChart() {
  _classCallCheck(this, AreaChart);

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  charts.add(['AreaChart', args]);
};

var PieChart = function PieChart() {
  _classCallCheck(this, PieChart);

  for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    args[_key2] = arguments[_key2];
  }

  charts.add(['PieChart', args]);
};

var LineChart = function LineChart() {
  _classCallCheck(this, LineChart);

  for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    args[_key3] = arguments[_key3];
  }

  charts.add(['LineChart', args]);
};

var ColumnChart = function ColumnChart() {
  _classCallCheck(this, ColumnChart);

  for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
    args[_key4] = arguments[_key4];
  }

  charts.add(['ColumnChart', args]);
};

var charts = new Set();

function initialize() {
  var _iterator = _createForOfIteratorHelper(charts),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var _ref3 = _step.value;

      var _ref2 = _slicedToArray(_ref3, 2);

      var ChartType = _ref2[0];
      var args = _ref2[1];

      _construct(window.Chartkick[ChartType], _toConsumableArray(args));
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  charts.clear();
}

if (!window.Chartkick) {
  window.Chartkick = {
    AreaChart: AreaChart,
    PieChart: PieChart,
    LineChart: LineChart,
    ColumnChart: ColumnChart
  };
  addEventListener('chartkick:ready', initialize);
}

/***/ }),

/***/ "./app/javascript/new_design/dossiers/auto-save-controller.js":
/*!********************************************************************!*\
  !*** ./app/javascript/new_design/dossiers/auto-save-controller.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AutoSaveController; });
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_promise_finally__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.promise.finally */ "./node_modules/core-js/modules/es.promise.finally.js");
/* harmony import */ var core_js_modules_es_promise_finally__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise_finally__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");
















function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

 // Manages a queue of Autosave operations,
// and sends `autosave:*` events to indicate the state of the requests.

var AutoSaveController = /*#__PURE__*/function () {
  function AutoSaveController() {
    _classCallCheck(this, AutoSaveController);

    this.timeoutDelay = 60000; // 1mn

    this.latestPromise = Promise.resolve();
  } // Add a new autosave request to the queue.
  // It will be started after the previous one finishes (to prevent older form data
  // to overwrite newer data if the server does not repond in order.)


  _createClass(AutoSaveController, [{
    key: "enqueueAutosaveRequest",
    value: function enqueueAutosaveRequest(form) {
      var _this = this;

      this.latestPromise = this.latestPromise["finally"](function () {
        return _this._sendAutosaveRequest(form).then(_this._didSucceed)["catch"](_this._didFail);
      });

      this._didEnqueue();
    } // Create a fetch request that saves the form.
    // Returns a promise fulfilled when the request completes.

  }, {
    key: "_sendAutosaveRequest",
    value: function _sendAutosaveRequest(form) {
      var _this2 = this;

      var autosavePromise = new Promise(function (resolve, reject) {
        if (!document.body.contains(form)) {
          return reject(new Error('The form can no longer be found.'));
        }

        var _this2$_formDataForDr = _this2._formDataForDraft(form),
            _this2$_formDataForDr2 = _slicedToArray(_this2$_formDataForDr, 2),
            formData = _this2$_formDataForDr2[0],
            formDataError = _this2$_formDataForDr2[1];

        if (formDataError) {
          formDataError.message = "Error while generating the form data (".concat(formDataError.message, ")");
          return reject(formDataError);
        }

        var params = {
          url: form.action,
          type: form.method,
          data: formData,
          dataType: 'script'
        };
        return Object(_utils__WEBPACK_IMPORTED_MODULE_15__["ajax"])(params).then(function (_ref) {
          var response = _ref.response;
          resolve(response);
        })["catch"](function (error) {
          reject(error);
        });
      }); // Time out the request after a while, to avoid recent requests not starting
      // because an older one is stuck.

      return Object(_utils__WEBPACK_IMPORTED_MODULE_15__["timeoutable"])(autosavePromise, this.timeoutDelay);
    } // Extract a FormData object of the form fields.

  }, {
    key: "_formDataForDraft",
    value: function _formDataForDraft(form) {
      // File inputs are handled separatly by ActiveStorage:
      // exclude them from the draft (by disabling them).
      // (Also Safari has issue with FormData containing empty file inputs)
      var fileInputs = form.querySelectorAll('input[type="file"]:not([disabled]), .editable-champ-piece_justificative input:not([disabled])');
      fileInputs.forEach(function (fileInput) {
        return fileInput.disabled = true;
      }); // Generate the form data

      var formData = null;

      try {
        formData = new FormData(form);
        return [formData, null];
      } catch (error) {
        return [null, error];
      } finally {
        // Re-enable disabled file inputs
        fileInputs.forEach(function (fileInput) {
          return fileInput.disabled = false;
        });
      }
    }
  }, {
    key: "_didEnqueue",
    value: function _didEnqueue() {
      Object(_utils__WEBPACK_IMPORTED_MODULE_15__["fire"])(document, 'autosave:enqueue');
    }
  }, {
    key: "_didSucceed",
    value: function _didSucceed(response) {
      Object(_utils__WEBPACK_IMPORTED_MODULE_15__["fire"])(document, 'autosave:end', response);
    }
  }, {
    key: "_didFail",
    value: function _didFail(error) {
      Object(_utils__WEBPACK_IMPORTED_MODULE_15__["fire"])(document, 'autosave:error', error);
    }
  }]);

  return AutoSaveController;
}();



/***/ }),

/***/ "./app/javascript/new_design/dossiers/auto-save.js":
/*!*********************************************************!*\
  !*** ./app/javascript/new_design/dossiers/auto-save.js ***!
  \*********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _auto_save_controller_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auto-save-controller.js */ "./app/javascript/new_design/dossiers/auto-save-controller.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");



var AUTOSAVE_DEBOUNCE_DELAY = gon.autosave.debounce_delay;
var AUTOSAVE_STATUS_VISIBLE_DURATION = gon.autosave.status_visible_duration; // Create a controller responsible for queuing autosave operations.

var autoSaveController = new _auto_save_controller_js__WEBPACK_IMPORTED_MODULE_1__["default"]();

function enqueueAutosaveRequest() {
  var form = document.querySelector(FORM_SELECTOR);
  autoSaveController.enqueueAutosaveRequest(form);
} //
// Whenever a 'change' event is triggered on one of the form inputs, try to autosave.
//


var FORM_SELECTOR = 'form#dossier-edit-form.autosave-enabled';
var INPUTS_SELECTOR = "".concat(FORM_SELECTOR, " input:not([type=file]), ").concat(FORM_SELECTOR, " select, ").concat(FORM_SELECTOR, " textarea");
var RETRY_BUTTON_SELECTOR = '.autosave-retry'; // When an autosave is requested programatically, auto-save the form immediately

addEventListener('autosave:trigger', function (event) {
  var form = event.target.closest('form');

  if (form && form.classList.contains('autosave-enabled')) {
    enqueueAutosaveRequest();
  }
}); // When the "Retry" button is clicked, auto-save the form immediately

Object(_utils__WEBPACK_IMPORTED_MODULE_2__["delegate"])('click', RETRY_BUTTON_SELECTOR, enqueueAutosaveRequest); // When an input changes, batches changes for N seconds, then auto-save the form

Object(_utils__WEBPACK_IMPORTED_MODULE_2__["delegate"])('change', INPUTS_SELECTOR, Object(_utils__WEBPACK_IMPORTED_MODULE_2__["debounce"])(enqueueAutosaveRequest, AUTOSAVE_DEBOUNCE_DELAY)); //
// Display some UI during the autosave
//

addEventListener('autosave:enqueue', function () {
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["disable"])(document.querySelector('button.autosave-retry'));
});
addEventListener('autosave:end', function () {
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["enable"])(document.querySelector('button.autosave-retry'));
  setState('succeeded');
  hideSucceededStatusAfterDelay();
});
addEventListener('autosave:error', function (event) {
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["enable"])(document.querySelector('button.autosave-retry'));
  setState('failed');
  logError(event.detail);
});

function setState(state) {
  var autosave = document.querySelector('.autosave');

  if (autosave) {
    // Re-apply the state even if already present, to get a nice animation
    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(autosave, 'autosave-state-idle');
    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(autosave, 'autosave-state-succeeded');
    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(autosave, 'autosave-state-failed');
    autosave.offsetHeight; // flush animations

    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["addClass"])(autosave, "autosave-state-".concat(state));
  }
}

function hideSucceededStatus() {
  var autosave = document.querySelector('.autosave');

  if (Object(_utils__WEBPACK_IMPORTED_MODULE_2__["hasClass"])(autosave, 'autosave-state-succeeded')) {
    setState('idle');
  }
}

var hideSucceededStatusAfterDelay = Object(_utils__WEBPACK_IMPORTED_MODULE_2__["debounce"])(hideSucceededStatus, AUTOSAVE_STATUS_VISIBLE_DURATION);

function logError(error) {
  if (error && error.message) {
    error.message = "[Autosave] ".concat(error.message);
    console.error(error);
    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["fire"])(document, 'sentry:capture-exception', error);
  }
}

/***/ }),

/***/ "./app/javascript/new_design/dossiers/auto-upload-controller.js":
/*!**********************************************************************!*\
  !*** ./app/javascript/new_design/dossiers/auto-upload-controller.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AutoUploadController; });
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _shared_activestorage_uploader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/activestorage/uploader */ "./app/javascript/shared/activestorage/uploader.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");
/* harmony import */ var _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shared/activestorage/file-upload-error */ "./app/javascript/shared/activestorage/file-upload-error.js");








function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



 // Given a file input in a champ with a selected file, upload a file,
// then attach it to the dossier.
//
// On success, the champ is replaced by an HTML fragment describing the attachment.
// On error, a error message is displayed above the input.

var AutoUploadController = /*#__PURE__*/function () {
  function AutoUploadController(input, file) {
    _classCallCheck(this, AutoUploadController);

    this.input = input;
    this.file = file;
    this.uploader = new _shared_activestorage_uploader__WEBPACK_IMPORTED_MODULE_7__["default"](input, file, input.dataset.directUploadUrl, input.dataset.autoAttachUrl);
  } // Create, upload and attach the file.
  // On failure, display an error message and throw a FileUploadError.


  _createClass(AutoUploadController, [{
    key: "start",
    value: function () {
      var _start = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_5___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_5___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                this._begin();

                _context.next = 4;
                return this.uploader.start();

              case 4:
                this._succeeded();

                _context.next = 11;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);

                this._failed(_context.t0);

                throw _context.t0;

              case 11:
                _context.prev = 11;

                this._done();

                return _context.finish(11);

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7, 11, 14]]);
      }));

      function start() {
        return _start.apply(this, arguments);
      }

      return start;
    }()
  }, {
    key: "_begin",
    value: function _begin() {
      this.input.disabled = true;

      this._hideErrorMessage();
    }
  }, {
    key: "_succeeded",
    value: function _succeeded() {
      this.input.value = null;
    }
  }, {
    key: "_failed",
    value: function _failed(error) {
      if (!document.body.contains(this.input)) {
        return;
      }

      this.uploader.progressBar.destroy();

      var message = this._messageFromError(error);

      this._displayErrorMessage(message);
    }
  }, {
    key: "_done",
    value: function _done() {
      this.input.disabled = false;
    }
  }, {
    key: "_messageFromError",
    value: function _messageFromError(error) {
      var message = error.message || error.toString();
      var canRetry = error.status && error.status != 422;

      if (error.failureReason == _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_9__["FAILURE_CONNECTIVITY"]) {
        return {
          title: 'Le fichier n’a pas pu être envoyé.',
          description: 'Vérifiez votre connexion à Internet, puis ré-essayez.',
          retry: true
        };
      } else if (error.code == _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_9__["ERROR_CODE_READ"]) {
        return {
          title: 'Nous n’arrivons pas à lire ce fichier sur votre appareil.',
          description: 'Essayez à nouveau, ou sélectionnez un autre fichier.',
          retry: false
        };
      } else {
        return {
          title: 'Le fichier n’a pas pu être envoyé.',
          description: message,
          retry: canRetry
        };
      }
    }
  }, {
    key: "_displayErrorMessage",
    value: function _displayErrorMessage(message) {
      var errorNode = this.input.parentElement.querySelector('.attachment-error');

      if (errorNode) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_8__["show"])(errorNode);
        errorNode.querySelector('.attachment-error-title').textContent = message.title || '';
        errorNode.querySelector('.attachment-error-description').textContent = message.description || '';
        Object(_utils__WEBPACK_IMPORTED_MODULE_8__["toggle"])(errorNode.querySelector('.attachment-error-retry'), message.retry);
      }
    }
  }, {
    key: "_hideErrorMessage",
    value: function _hideErrorMessage() {
      var errorElement = this.input.parentElement.querySelector('.attachment-error');

      if (errorElement) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_8__["hide"])(errorElement);
      }
    }
  }]);

  return AutoUploadController;
}();



/***/ }),

/***/ "./app/javascript/new_design/dossiers/auto-upload.js":
/*!***********************************************************!*\
  !*** ./app/javascript/new_design/dossiers/auto-upload.js ***!
  \***********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _auto_uploads_controllers_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auto-uploads-controllers.js */ "./app/javascript/new_design/dossiers/auto-uploads-controllers.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");





 // Create a controller responsible for managing several concurrent uploads.

var autoUploadsControllers = new _auto_uploads_controllers_js__WEBPACK_IMPORTED_MODULE_4__["default"]();

function startUpload(input) {
  Array.from(input.files).forEach(function (file) {
    autoUploadsControllers.upload(input, file);
  });
}

var fileInputSelector = "input[type=file][data-direct-upload-url][data-auto-attach-url]:not([disabled])";
Object(_utils__WEBPACK_IMPORTED_MODULE_5__["delegate"])('change', fileInputSelector, function (event) {
  startUpload(event.target);
});
var retryButtonSelector = "button.attachment-error-retry";
Object(_utils__WEBPACK_IMPORTED_MODULE_5__["delegate"])('click', retryButtonSelector, function () {
  var inputSelector = this.dataset.inputTarget;
  var input = document.querySelector(inputSelector);
  startUpload(input);
});

/***/ }),

/***/ "./app/javascript/new_design/dossiers/auto-uploads-controllers.js":
/*!************************************************************************!*\
  !*** ./app/javascript/new_design/dossiers/auto-uploads-controllers.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AutoUploadsControllers; });
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @rails/ujs */ "./node_modules/@rails/ujs/lib/assets/compiled/rails-ujs.js");
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_rails_ujs__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _auto_upload_controller_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auto-upload-controller.js */ "./app/javascript/new_design/dossiers/auto-upload-controller.js");
/* harmony import */ var _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../shared/activestorage/file-upload-error */ "./app/javascript/shared/activestorage/file-upload-error.js");







function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



 // Manage multiple concurrent uploads.
//
// When the first upload starts, all the form "Submit" buttons are disabled.
// They are enabled again when the last upload ends.

var AutoUploadsControllers = /*#__PURE__*/function () {
  function AutoUploadsControllers() {
    _classCallCheck(this, AutoUploadsControllers);

    this.inFlightUploadsCount = 0;
  }

  _createClass(AutoUploadsControllers, [{
    key: "upload",
    value: function () {
      var _upload = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_4___default.a.mark(function _callee(input, file) {
        var form, controller;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_4___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                form = input.form;

                this._incrementInFlightUploads(form);

                _context.prev = 2;
                controller = new _auto_upload_controller_js__WEBPACK_IMPORTED_MODULE_7__["default"](input, file);
                _context.next = 6;
                return controller.start();

              case 6:
                _context.next = 12;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](2);

                if (!(_context.t0.failureReason == _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_8__["FAILURE_CLIENT"] && _context.t0.code != _shared_activestorage_file_upload_error__WEBPACK_IMPORTED_MODULE_8__["ERROR_CODE_READ"])) {
                  _context.next = 12;
                  break;
                }

                throw _context.t0;

              case 12:
                _context.prev = 12;

                this._decrementInFlightUploads(form);

                return _context.finish(12);

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[2, 8, 12, 15]]);
      }));

      function upload(_x, _x2) {
        return _upload.apply(this, arguments);
      }

      return upload;
    }()
  }, {
    key: "_incrementInFlightUploads",
    value: function _incrementInFlightUploads(form) {
      this.inFlightUploadsCount += 1;

      if (form) {
        form.querySelectorAll('button[type=submit]').forEach(function (submitButton) {
          return _rails_ujs__WEBPACK_IMPORTED_MODULE_6___default.a.disableElement(submitButton);
        });
      }
    }
  }, {
    key: "_decrementInFlightUploads",
    value: function _decrementInFlightUploads(form) {
      if (this.inFlightUploadsCount > 0) {
        this.inFlightUploadsCount -= 1;
      }

      if (this.inFlightUploadsCount == 0 && form) {
        form.querySelectorAll('button[type=submit]').forEach(function (submitButton) {
          return _rails_ujs__WEBPACK_IMPORTED_MODULE_6___default.a.enableElement(submitButton);
        });
      }
    }
  }]);

  return AutoUploadsControllers;
}();



/***/ }),

/***/ "./app/javascript/new_design/dropdown.js":
/*!***********************************************!*\
  !*** ./app/javascript/new_design/dropdown.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");














function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('click', 'body', function (event) {
  if (!event.target.closest('.dropdown')) {
    _toConsumableArray(document.querySelectorAll('.dropdown')).forEach(function (element) {
      var button = element.querySelector('.dropdown-button');
      button.setAttribute('aria-expanded', false);
      element.classList.remove('open', 'fade-in-down');
    });
  }
});
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('click', '.dropdown-button', function (event) {
  event.stopPropagation();
  var button = event.target.closest('.dropdown-button');
  var parent = button.parentElement;

  if (parent.classList.contains('dropdown')) {
    parent.classList.toggle('open');
    var buttonExpanded = button.getAttribute('aria-expanded') === 'true';
    button.setAttribute('aria-expanded', !buttonExpanded);
  }
});

/***/ }),

/***/ "./app/javascript/new_design/form-validation.js":
/*!******************************************************!*\
  !*** ./app/javascript/new_design/form-validation.js ***!
  \******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");














function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('blur keydown', 'input, textarea', function (_ref) {
  var target = _ref.target;
  touch(target);
});
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('click', 'input[type="submit"]:not([formnovalidate])', function (_ref2) {
  var target = _ref2.target;
  var form = target.closest('form');
  var inputs = form ? form.querySelectorAll('input, textarea') : [];

  _toConsumableArray(inputs).forEach(touch);
});

function touch(_ref3) {
  var classList = _ref3.classList;
  classList.add('touched');
}

/***/ }),

/***/ "./app/javascript/new_design/procedure-context.js":
/*!********************************************************!*\
  !*** ./app/javascript/new_design/procedure-context.js ***!
  \********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");


function updateReadMoreVisibility() {
  var descBody = document.querySelector('.procedure-description-body');

  if (descBody) {
    // If the description text overflows, display a "Read more" button.
    var isOverflowing = descBody.scrollHeight > descBody.clientHeight;
    descBody.classList.toggle('read-more-enabled', isOverflowing);
  }
}

function expandProcedureDescription() {
  var descBody = document.querySelector('.procedure-description-body');
  descBody.classList.remove('read-more-collapsed');
}

addEventListener('ds:page:update', updateReadMoreVisibility);
addEventListener('resize', updateReadMoreVisibility);
Object(_utils__WEBPACK_IMPORTED_MODULE_0__["delegate"])('click', '.read-more-button', expandProcedureDescription);

/***/ }),

/***/ "./app/javascript/new_design/procedure-form.js":
/*!*****************************************************!*\
  !*** ./app/javascript/new_design/procedure-form.js ***!
  \*****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");


function syncInputToElement(fromSelector, toSelector) {
  var fromElement = document.querySelector(fromSelector);
  var toElement = document.querySelector(toSelector);

  if (toElement && fromElement) {
    toElement.innerText = fromElement.value;
  }
}

function syncFormToPreview() {
  syncInputToElement('#procedure_libelle', '.procedure-title');
  syncInputToElement('#procedure_description', '.procedure-description-body');
}

Object(_utils__WEBPACK_IMPORTED_MODULE_0__["delegate"])('input', '.procedure-form #procedure_libelle', syncFormToPreview);
Object(_utils__WEBPACK_IMPORTED_MODULE_0__["delegate"])('input', '.procedure-form #procedure_description', syncFormToPreview);

/***/ }),

/***/ "./app/javascript/new_design/select2.js":
/*!**********************************************!*\
  !*** ./app/javascript/new_design/select2.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var select2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! select2 */ "./node_modules/select2/dist/js/select2.js");
/* harmony import */ var select2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(select2__WEBPACK_IMPORTED_MODULE_1__);


var language = {
  errorLoading: function errorLoading() {
    return 'Les résultats ne peuvent pas être chargés.';
  },
  inputTooLong: function inputTooLong(args) {
    var overChars = args.input.length - args.maximum;
    return 'Supprimez ' + overChars + ' caractère' + (overChars > 1 ? 's' : '');
  },
  inputTooShort: function inputTooShort(args) {
    var remainingChars = args.minimum - args.input.length;
    return 'Saisissez au moins ' + remainingChars + ' caractère' + (remainingChars > 1 ? 's' : '');
  },
  loadingMore: function loadingMore() {
    return 'Chargement de résultats supplémentaires…';
  },
  maximumSelected: function maximumSelected(args) {
    return 'Vous pouvez seulement sélectionner ' + args.maximum + ' élément' + (args.maximum > 1 ? 's' : '');
  },
  noResults: function noResults() {
    return 'Aucun résultat trouvé';
  },
  searching: function searching() {
    return 'Recherche en cours…';
  },
  removeAllItems: function removeAllItems() {
    return 'Supprimer tous les éléments';
  }
};
var baseOptions = {
  language: language,
  width: '100%'
};

var templateOption = function templateOption(_ref) {
  var text = _ref.text;
  return jquery__WEBPACK_IMPORTED_MODULE_0___default()("<span class=\"custom-select2-option\"><span class=\"icon person\"></span>".concat(text, "</span>"));
};

addEventListener('ds:page:update', function () {
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('select.select2').select2(baseOptions);
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('.columns-form select.select2-limited').select2({
    width: '300px',
    placeholder: 'Sélectionnez des colonnes',
    maximumSelectionLength: '5'
  });
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('.recipients-form select.select2-limited').select2({
    language: language,
    width: '300px',
    placeholder: 'Sélectionnez des instructeurs',
    maximumSelectionLength: '30'
  });
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('select.select2-limited.select-instructeurs').select2({
    language: language,
    dropdownParent: jquery__WEBPACK_IMPORTED_MODULE_0___default()('.instructeur-wrapper'),
    placeholder: 'Saisir l’adresse email de l’instructeur',
    tags: true,
    tokenSeparators: [',', ' '],
    templateResult: templateOption,
    templateSelection: templateOption
  });
});

/***/ }),

/***/ "./app/javascript/new_design/spinner.js":
/*!**********************************************!*\
  !*** ./app/javascript/new_design/spinner.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");














function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



function showSpinner() {
  _toConsumableArray(document.querySelectorAll('.spinner')).forEach(_utils__WEBPACK_IMPORTED_MODULE_13__["show"]);
}

function hideSpinner() {
  _toConsumableArray(document.querySelectorAll('.spinner')).forEach(_utils__WEBPACK_IMPORTED_MODULE_13__["hide"]);
}

Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('ajax:complete', '[data-spinner]', hideSpinner);
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('ajax:stopped', '[data-spinner]', hideSpinner);
Object(_utils__WEBPACK_IMPORTED_MODULE_13__["delegate"])('ajax:send', '[data-spinner]', showSpinner);

/***/ }),

/***/ "./app/javascript/new_design/state-button.js":
/*!***************************************************!*\
  !*** ./app/javascript/new_design/state-button.js ***!
  \***************************************************/
/*! exports provided: showMotivation, motivationCancel, showImportJustificatif */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showMotivation", function() { return showMotivation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "motivationCancel", function() { return motivationCancel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showImportJustificatif", function() { return showImportJustificatif; });
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");



function showMotivation(event, state) {
  event.preventDefault();
  motivationCancel();
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["show"])(document.querySelector(".motivation.".concat(state)));
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["hide"])(document.querySelector('.dropdown-items'));
}
function motivationCancel() {
  document.querySelectorAll('.motivation').forEach(_utils__WEBPACK_IMPORTED_MODULE_2__["hide"]);
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["show"])(document.querySelector('.dropdown-items'));
}
function showImportJustificatif(name) {
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["show"])(document.querySelector('#justificatif_motivation_import_' + name));
  Object(_utils__WEBPACK_IMPORTED_MODULE_2__["hide"])(document.querySelector('#justificatif_motivation_suggest_' + name));
}

/***/ }),

/***/ "./app/javascript/new_design/support.js":
/*!**********************************************!*\
  !*** ./app/javascript/new_design/support.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_object_freeze__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.object.freeze */ "./node_modules/core-js/modules/es.object.freeze.js");
/* harmony import */ var core_js_modules_es_object_freeze__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_freeze__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__);




function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//
// This content is inspired by w3c aria example
// https://www.w3.org/TR/wai-aria-practices-1.1/examples/disclosure/disclosure-faq.html
//
var ButtonExpand = /*#__PURE__*/function () {
  function ButtonExpand(domNode) {
    _classCallCheck(this, ButtonExpand);

    this.handleFocus = function () {
      this.domNode.classList.add('focus');
    };

    this.domNode = domNode;
    this.keyCode = Object.freeze({
      RETURN: 13
    });
    this.allButtons = [];
    this.controlledNode = false;
    var id = this.domNode.getAttribute('aria-controls');

    if (id) {
      this.controlledNode = document.getElementById(id);
    }

    this.domNode.setAttribute('aria-expanded', 'false');
    this.hideContent();
    this.domNode.addEventListener('keydown', this.handleKeydown.bind(this));
    this.domNode.addEventListener('click', this.handleClick.bind(this));
    this.domNode.addEventListener('focus', this.handleFocus.bind(this));
    this.domNode.addEventListener('blur', this.handleBlur.bind(this));
  }

  _createClass(ButtonExpand, [{
    key: "showContent",
    value: function showContent() {
      var _this = this;

      this.domNode.setAttribute('aria-expanded', 'true');
      this.domNode.classList.add('primary');

      if (this.controlledNode) {
        this.controlledNode.classList.remove('hidden');
      }

      this.formInput.value = this.domNode.getAttribute('data-question-type');
      this.allButtons.forEach(function (b) {
        if (b != _this) {
          b.hideContent();
        }
      });
    }
  }, {
    key: "hideContent",
    value: function hideContent() {
      this.domNode.setAttribute('aria-expanded', 'false');
      this.domNode.classList.remove('primary');

      if (this.controlledNode) {
        this.controlledNode.classList.add('hidden');
      }
    }
  }, {
    key: "toggleExpand",
    value: function toggleExpand() {
      if (this.domNode.getAttribute('aria-expanded') === 'true') {
        this.hideContent();
      } else {
        this.showContent();
      }
    }
  }, {
    key: "setAllButtons",
    value: function setAllButtons(buttons) {
      this.allButtons = buttons;
    }
  }, {
    key: "setFormInput",
    value: function setFormInput(formInput) {
      this.formInput = formInput;
    }
  }, {
    key: "handleKeydown",
    value: function handleKeydown() {
      switch (event.keyCode) {
        case this.keyCode.RETURN:
          this.toggleExpand();
          event.stopPropagation();
          event.preventDefault();
          break;

        default:
          break;
      }
    }
  }, {
    key: "handleClick",
    value: function handleClick() {
      event.stopPropagation();
      event.preventDefault();
      this.toggleExpand();
    }
  }, {
    key: "handleBlur",
    value: function handleBlur() {
      this.domNode.classList.remove('focus');
    }
  }]);

  return ButtonExpand;
}();
/* Initialize Hide/Show Buttons */


if (document.querySelector('#contact-form')) {
  window.addEventListener('ds:page:update', function () {
    var buttons = document.querySelectorAll('button[aria-expanded][aria-controls], button.button-without-hint');
    var expandButtons = [];
    var formInput = document.querySelector('form input#type');
    buttons.forEach(function (button) {
      var be = new ButtonExpand(button);
      expandButtons.push(be);
    });
    expandButtons.forEach(function (button) {
      return button.setAllButtons(expandButtons);
    });
    expandButtons.forEach(function (button) {
      return button.setFormInput(formInput);
    });
  }, false);
}

/***/ }),

/***/ "./app/javascript/new_design/user-sign_up.js":
/*!***************************************************!*\
  !*** ./app/javascript/new_design/user-sign_up.js ***!
  \***************************************************/
/*! exports provided: acceptEmailSuggestion, discardEmailSuggestionBox */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "acceptEmailSuggestion", function() { return acceptEmailSuggestion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "discardEmailSuggestionBox", function() { return discardEmailSuggestionBox; });
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");
/* harmony import */ var email_butler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! email-butler */ "./node_modules/email-butler/lib/index.js");
/* harmony import */ var email_butler__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(email_butler__WEBPACK_IMPORTED_MODULE_1__);


var userNewEmailSelector = '#new_user input#user_email';
var passwordFieldSelector = '#new_user input#user_password';
var suggestionsSelector = '.suspect-email';
var emailSuggestionSelector = '.suspect-email .email-suggestion-address';
Object(_utils__WEBPACK_IMPORTED_MODULE_0__["delegate"])('focusout', userNewEmailSelector, function () {
  // When the user leaves the email input during account creation, we check if this account looks correct.
  // If not (e.g if its "bidou@gmail.coo" or "john@yahoo.rf"), we attempt to suggest a fix for the invalid email.
  var userEmailInput = document.querySelector(userNewEmailSelector);
  var suggestedEmailSpan = document.querySelector(emailSuggestionSelector);
  var suggestion = Object(email_butler__WEBPACK_IMPORTED_MODULE_1__["suggest"])(userEmailInput.value);

  if (suggestion && suggestion.full && suggestedEmailSpan) {
    suggestedEmailSpan.innerHTML = suggestion.full;
    Object(_utils__WEBPACK_IMPORTED_MODULE_0__["show"])(document.querySelector(suggestionsSelector));
  } else {
    Object(_utils__WEBPACK_IMPORTED_MODULE_0__["hide"])(document.querySelector(suggestionsSelector));
  }
});
function acceptEmailSuggestion() {
  var userEmailInput = document.querySelector(userNewEmailSelector);
  var suggestedEmailSpan = document.querySelector(emailSuggestionSelector);
  userEmailInput.value = suggestedEmailSpan.innerHTML;
  Object(_utils__WEBPACK_IMPORTED_MODULE_0__["hide"])(document.querySelector(suggestionsSelector));
  document.querySelector(passwordFieldSelector).focus();
}
function discardEmailSuggestionBox() {
  Object(_utils__WEBPACK_IMPORTED_MODULE_0__["hide"])(document.querySelector(suggestionsSelector));
}

/***/ }),

/***/ "./app/javascript/packs/application.js":
/*!*********************************************!*\
  !*** ./app/javascript/packs/application.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_polyfills__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../shared/polyfills */ "./app/javascript/shared/polyfills.js");
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @rails/ujs */ "./node_modules/@rails/ujs/lib/assets/compiled/rails-ujs.js");
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_rails_ujs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _rails_activestorage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @rails/activestorage */ "./node_modules/@rails/activestorage/app/assets/javascripts/activestorage.js");
/* harmony import */ var _rails_activestorage__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_rails_activestorage__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! whatwg-fetch */ "./node_modules/whatwg-fetch/fetch.js");
/* harmony import */ var react_ujs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react_ujs */ "./node_modules/react_ujs/react_ujs/index.js");
/* harmony import */ var react_ujs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_ujs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _shared_page_update_event__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/page-update-event */ "./app/javascript/shared/page-update-event.js");
/* harmony import */ var _shared_activestorage_ujs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/activestorage/ujs */ "./app/javascript/shared/activestorage/ujs.js");
/* harmony import */ var _shared_remote_poller__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/remote-poller */ "./app/javascript/shared/remote-poller.js");
/* harmony import */ var _shared_safari_11_file_xhr_workaround__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/safari-11-file-xhr-workaround */ "./app/javascript/shared/safari-11-file-xhr-workaround.js");
/* harmony import */ var _shared_remote_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/remote-input */ "./app/javascript/shared/remote-input.js");
/* harmony import */ var _shared_franceconnect__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/franceconnect */ "./app/javascript/shared/franceconnect.js");
/* harmony import */ var _shared_toggle_target__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/toggle-target */ "./app/javascript/shared/toggle-target.js");
/* harmony import */ var _new_design_chartkick__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../new_design/chartkick */ "./app/javascript/new_design/chartkick.js");
/* harmony import */ var _new_design_dropdown__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../new_design/dropdown */ "./app/javascript/new_design/dropdown.js");
/* harmony import */ var _new_design_form_validation__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../new_design/form-validation */ "./app/javascript/new_design/form-validation.js");
/* harmony import */ var _new_design_procedure_context__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../new_design/procedure-context */ "./app/javascript/new_design/procedure-context.js");
/* harmony import */ var _new_design_procedure_form__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../new_design/procedure-form */ "./app/javascript/new_design/procedure-form.js");
/* harmony import */ var _new_design_select2__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../new_design/select2 */ "./app/javascript/new_design/select2.js");
/* harmony import */ var _new_design_spinner__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../new_design/spinner */ "./app/javascript/new_design/spinner.js");
/* harmony import */ var _new_design_support__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../new_design/support */ "./app/javascript/new_design/support.js");
/* harmony import */ var _new_design_dossiers_auto_save__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../new_design/dossiers/auto-save */ "./app/javascript/new_design/dossiers/auto-save.js");
/* harmony import */ var _new_design_dossiers_auto_upload__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../new_design/dossiers/auto-upload */ "./app/javascript/new_design/dossiers/auto-upload.js");
/* harmony import */ var _new_design_champs_carte__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../new_design/champs/carte */ "./app/javascript/new_design/champs/carte.js");
/* harmony import */ var _new_design_champs_linked_drop_down_list__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../new_design/champs/linked-drop-down-list */ "./app/javascript/new_design/champs/linked-drop-down-list.js");
/* harmony import */ var _new_design_champs_repetition__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../new_design/champs/repetition */ "./app/javascript/new_design/champs/repetition.js");
/* harmony import */ var _new_design_avis__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../new_design/avis */ "./app/javascript/new_design/avis.js");
/* harmony import */ var _new_design_state_button__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../new_design/state-button */ "./app/javascript/new_design/state-button.js");
/* harmony import */ var _new_design_user_sign_up__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../new_design/user-sign_up */ "./app/javascript/new_design/user-sign_up.js");



 // window.fetch polyfill
























 // This is the global application namespace where we expose helpers used from rails views

var DS = {
  fire: function fire(eventName, data) {
    return _rails_ujs__WEBPACK_IMPORTED_MODULE_1___default.a.fire(document, eventName, data);
  },
  toggleCondidentielExplanation: _new_design_avis__WEBPACK_IMPORTED_MODULE_25__["toggleCondidentielExplanation"],
  showMotivation: _new_design_state_button__WEBPACK_IMPORTED_MODULE_26__["showMotivation"],
  motivationCancel: _new_design_state_button__WEBPACK_IMPORTED_MODULE_26__["motivationCancel"],
  showImportJustificatif: _new_design_state_button__WEBPACK_IMPORTED_MODULE_26__["showImportJustificatif"],
  replaceSemicolonByComma: _new_design_avis__WEBPACK_IMPORTED_MODULE_25__["replaceSemicolonByComma"],
  acceptEmailSuggestion: _new_design_user_sign_up__WEBPACK_IMPORTED_MODULE_27__["acceptEmailSuggestion"],
  discardEmailSuggestionBox: _new_design_user_sign_up__WEBPACK_IMPORTED_MODULE_27__["discardEmailSuggestionBox"]
}; // Start Rails helpers

_rails_ujs__WEBPACK_IMPORTED_MODULE_1___default.a.start();
_rails_activestorage__WEBPACK_IMPORTED_MODULE_2__["start"](); // Expose globals

window.DS = window.DS || DS; // eslint-disable-next-line no-undef, react-hooks/rules-of-hooks

react_ujs__WEBPACK_IMPORTED_MODULE_4___default.a.useContext(__webpack_require__("./app/javascript/loaders sync recursive ^\\.\\/.*$"));
addEventListener('ds:page:update', react_ujs__WEBPACK_IMPORTED_MODULE_4___default.a.handleMount);

/***/ }),

/***/ "./app/javascript/shared/activestorage/uploader.js":
/*!*********************************************************!*\
  !*** ./app/javascript/shared/activestorage/uploader.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Uploader; });
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _rails_activestorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @rails/activestorage */ "./node_modules/@rails/activestorage/app/assets/javascripts/activestorage.js");
/* harmony import */ var _rails_activestorage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_rails_activestorage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");
/* harmony import */ var _progress_bar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./progress-bar */ "./app/javascript/shared/activestorage/progress-bar.js");
/* harmony import */ var _file_upload_error__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./file-upload-error */ "./app/javascript/shared/activestorage/file-upload-error.js");





function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





/**
  Uploader class is a delegate for DirectUpload instance
  used to track lifecycle and progress of an upload.
  */

var Uploader = /*#__PURE__*/function () {
  function Uploader(input, file, directUploadUrl, autoAttachUrl) {
    _classCallCheck(this, Uploader);

    this.directUpload = new _rails_activestorage__WEBPACK_IMPORTED_MODULE_4__["DirectUpload"](file, directUploadUrl, this);
    this.progressBar = new _progress_bar__WEBPACK_IMPORTED_MODULE_6__["default"](input, this.directUpload.id, file);
    this.autoAttachUrl = autoAttachUrl;
  }
  /**
    Upload (and optionally attach) the file.
    Returns the blob signed id on success.
    Throws a FileUploadError on failure.
    */


  _createClass(Uploader, [{
    key: "start",
    value: function () {
      var _start = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee() {
        var blobSignedId;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.progressBar.start();
                _context.prev = 1;
                _context.next = 4;
                return this._upload();

              case 4:
                blobSignedId = _context.sent;

                if (!this.autoAttachUrl) {
                  _context.next = 10;
                  break;
                }

                _context.next = 8;
                return this._attach(blobSignedId);

              case 8:
                _context.next = 12;
                break;

              case 10:
                this.progressBar.end();
                this.progressBar.destroy();

              case 12:
                return _context.abrupt("return", blobSignedId);

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](1);
                this.progressBar.error(_context.t0.message);
                throw _context.t0;

              case 19:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 15]]);
      }));

      function start() {
        return _start.apply(this, arguments);
      }

      return start;
    }()
    /**
      Upload the file using the DirectUpload instance, and return the blob signed_id.
      Throws a FileUploadError on failure.
      */

  }, {
    key: "_upload",
    value: function () {
      var _upload2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee2() {
        var _this = this;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                  _this.directUpload.create(function (errorMsg, attributes) {
                    if (errorMsg) {
                      var error = Object(_file_upload_error__WEBPACK_IMPORTED_MODULE_7__["errorFromDirectUploadMessage"])(errorMsg);
                      reject(error);
                    } else {
                      resolve(attributes.signed_id);
                    }
                  });
                }));

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function _upload() {
        return _upload2.apply(this, arguments);
      }

      return _upload;
    }()
    /**
      Attach the file by sending a POST request to the autoAttachUrl.
      Throws a FileUploadError on failure (containing the first validation
      error message, if any).
      */

  }, {
    key: "_attach",
    value: function () {
      var _attach2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee3(blobSignedId) {
        var attachmentRequest, message;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                attachmentRequest = {
                  url: this.autoAttachUrl,
                  type: 'PUT',
                  data: "blob_signed_id=".concat(blobSignedId)
                };
                _context3.prev = 1;
                _context3.next = 4;
                return Object(_utils__WEBPACK_IMPORTED_MODULE_5__["ajax"])(attachmentRequest);

              case 4:
                _context3.next = 10;
                break;

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](1);
                message = _context3.t0.response && _context3.t0.response.errors && _context3.t0.response.errors[0];
                throw new _file_upload_error__WEBPACK_IMPORTED_MODULE_7__["default"](message || 'Error attaching file.', _context3.t0.xhr.status, _file_upload_error__WEBPACK_IMPORTED_MODULE_7__["ERROR_CODE_ATTACH"]);

              case 10:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[1, 6]]);
      }));

      function _attach(_x) {
        return _attach2.apply(this, arguments);
      }

      return _attach;
    }()
  }, {
    key: "uploadRequestDidProgress",
    value: function uploadRequestDidProgress(event) {
      var progress = event.loaded / event.total * 100;

      if (progress) {
        this.progressBar.progress(progress);
      }
    }
  }, {
    key: "directUploadWillStoreFileWithXHR",
    value: function directUploadWillStoreFileWithXHR(xhr) {
      var _this2 = this;

      xhr.upload.addEventListener('progress', function (event) {
        return _this2.uploadRequestDidProgress(event);
      });
    }
  }]);

  return Uploader;
}();



/***/ }),

/***/ "./app/javascript/shared/remote-poller.js":
/*!************************************************!*\
  !*** ./app/javascript/shared/remote-poller.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.set */ "./node_modules/core-js/modules/es.set.js");
/* harmony import */ var core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_set__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");













function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


addEventListener('DOMContentLoaded', function () {
  attachementPoller.deactivate();
  exportPoller.deactivate();
  var attachments = document.querySelectorAll('[data-attachment-poll-url]');
  var exports = document.querySelectorAll('[data-export-poll-url]');

  var _iterator = _createForOfIteratorHelper(attachments),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var _ref3 = _step.value;
      var dataset = _ref3.dataset;
      attachementPoller.add(dataset.attachmentPollUrl);
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  var _iterator2 = _createForOfIteratorHelper(exports),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var _ref4 = _step2.value;
      var _dataset = _ref4.dataset;
      exportPoller.add(_dataset.exportPollUrl);
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }
});
addEventListener('attachment:update', function (_ref5) {
  var url = _ref5.detail.url;
  attachementPoller.add(url);
});
addEventListener('export:update', function (_ref6) {
  var url = _ref6.detail.url;
  exportPoller.add(url);
});
Object(_utils__WEBPACK_IMPORTED_MODULE_12__["delegate"])('click', '[data-attachment-refresh]', function (event) {
  event.preventDefault();
  attachementPoller.check();
}); // Periodically check the state of a set of URLs.
//
// Each time the given URL is requested, the matching `show.js.erb` view is rendered,
// causing the state to be refreshed.
//
// This is used mainly to refresh attachments during the anti-virus check,
// but also to refresh the state of a pending spreadsheet export.

var RemotePoller = /*#__PURE__*/function () {
  function RemotePoller() {
    var settings = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, RemotePoller);

    this.urls = new Set();
    this.timeout = void 0;
    this.checks = 0;
    this.interval = settings.interval;
    this.maxChecks = settings.maxChecks;
  }

  _createClass(RemotePoller, [{
    key: "add",
    value: function add(url) {
      if (this.isEnabled) {
        if (!this.isActive) {
          this.activate();
        }

        this.urls.add(url);
      }
    }
  }, {
    key: "check",
    value: function check() {
      var urls = this.urls;
      this.reset();

      var _iterator3 = _createForOfIteratorHelper(urls),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var url = _step3.value;
          // Start the request. The JS payload in the response will update the page.
          // (Errors are ignored, because background tasks shouldn't report errors to the user.)
          Object(_utils__WEBPACK_IMPORTED_MODULE_12__["ajax"])({
            url: url,
            type: 'get'
          })["catch"](function () {});
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }
  }, {
    key: "activate",
    value: function activate() {
      var _this = this;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(function () {
        _this.checks++;
        _this.currentInterval = _this.interval * 1.5;

        _this.check();
      }, this.currentInterval);
    }
  }, {
    key: "deactivate",
    value: function deactivate() {
      this.checks = 0;
      this.currentInterval = this.interval;
      this.reset();
    }
  }, {
    key: "reset",
    value: function reset() {
      clearTimeout(this.timeout);
      this.urls = new Set();
      this.timeout = undefined;
    }
  }, {
    key: "isEnabled",
    get: function get() {
      return this.checks <= this.maxChecks;
    }
  }, {
    key: "isActive",
    get: function get() {
      return this.timeout !== undefined;
    }
  }]);

  return RemotePoller;
}();

var attachementPoller = new RemotePoller({
  interval: 3000,
  maxChecks: 5
});
var exportPoller = new RemotePoller({
  interval: 6000,
  maxChecks: 10
});

/***/ }),

/***/ "./app/javascript/shared/toggle-target.js":
/*!************************************************!*\
  !*** ./app/javascript/shared/toggle-target.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");












function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

 // Unobtrusive Javascript for allowing an element to toggle
// the visibility of another element.
//
// Usage:
//   <button data-toggle-target="#target">Toggle</button>
//   <div id="target">Content</div>

var TOGGLE_SOURCE_SELECTOR = '[data-toggle-target]';
Object(_utils__WEBPACK_IMPORTED_MODULE_11__["delegate"])('click', TOGGLE_SOURCE_SELECTOR, function (evt) {
  evt.preventDefault();
  var targetSelector = evt.target.dataset.toggleTarget;
  var targetElements = document.querySelectorAll(targetSelector);

  var _iterator = _createForOfIteratorHelper(targetElements),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var target = _step.value;
      Object(_utils__WEBPACK_IMPORTED_MODULE_11__["toggle"])(target);
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
});

/***/ })

},[["./app/javascript/packs/application.js","runtime~application",2,10,4]]]);
//# sourceMappingURL=application-10242283eb642852ff5f.chunk.js.map