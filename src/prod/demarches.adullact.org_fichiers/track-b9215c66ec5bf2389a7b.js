(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["track"],{

/***/ "./app/javascript/packs/track.js":
/*!***************************************!*\
  !*** ./app/javascript/packs/track.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_track_matomo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../shared/track/matomo */ "./app/javascript/shared/track/matomo.js");
/* harmony import */ var _shared_track_matomo__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_shared_track_matomo__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _shared_track_sentry__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/track/sentry */ "./app/javascript/shared/track/sentry.js");



/***/ }),

/***/ "./app/javascript/shared/track/matomo.js":
/*!***********************************************!*\
  !*** ./app/javascript/shared/track/matomo.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var _ref = gon.matomo || {},
    key = _ref.key,
    enabled = _ref.enabled; // if (enabled) {


if (true) {
  window._paq = window._paq || [];
  var url = '//statistiques.adullact.org/';
  var trackerUrl = "".concat(url, "piwik.php");
  var jsUrl = "".concat(url, "piwik.js"); // Configure Matomo analytics

  window._paq.push(['setCookieDomain', '*.demarches.adullact.org']);

  window._paq.push(['setDomains', ['*.demarches.adullact.org']]);

  window._paq.push(['setDoNotTrack', true]);

  window._paq.push(['trackPageView']);

  window._paq.push(['enableLinkTracking']); // Load script from Matomo


  window._paq.push(['setTrackerUrl', trackerUrl]);

  window._paq.push(['setSiteId', '12']);

  var script = document.createElement('script');
  var firstScript = document.getElementsByTagName('script')[0];
  script.type = 'text/javascript';
  script.id = 'matomo-js';
  script.async = true;
  script.src = jsUrl;
  firstScript.parentNode.insertBefore(script, firstScript);
}

/***/ }),

/***/ "./app/javascript/shared/track/sentry.js":
/*!***********************************************!*\
  !*** ./app/javascript/shared/track/sentry.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sentry_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/browser */ "./node_modules/@sentry/browser/esm/index.js");


var _ref = gon.sentry || {},
    key = _ref.key,
    enabled = _ref.enabled,
    user = _ref.user,
    environment = _ref.environment,
    browser = _ref.browser; // We need to check for key presence here as we do not have a dsn for browser yet


if (enabled && key) {
  _sentry_browser__WEBPACK_IMPORTED_MODULE_0__["init"]({
    dsn: key,
    environment: environment
  });
  _sentry_browser__WEBPACK_IMPORTED_MODULE_0__["configureScope"](function (scope) {
    scope.setUser(user);
    scope.setExtra('browser', browser.modern ? 'modern' : 'legacy');
  }); // Register a way to explicitely capture messages from a different bundle.

  addEventListener('sentry:capture-exception', function (event) {
    var error = event.detail;
    _sentry_browser__WEBPACK_IMPORTED_MODULE_0__["captureException"](error);
  });
}

/***/ })

},[["./app/javascript/packs/track.js","runtime~track",8]]]);
//# sourceMappingURL=track-b9215c66ec5bf2389a7b.chunk.js.map