(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./app/javascript/shared/activestorage/file-upload-error.js":
/*!******************************************************************!*\
  !*** ./app/javascript/shared/activestorage/file-upload-error.js ***!
  \******************************************************************/
/*! exports provided: ERROR_CODE_READ, ERROR_CODE_CREATE, ERROR_CODE_STORE, ERROR_CODE_ATTACH, FAILURE_CLIENT, FAILURE_SERVER, FAILURE_CONNECTIVITY, default, errorFromDirectUploadMessage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERROR_CODE_READ", function() { return ERROR_CODE_READ; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERROR_CODE_CREATE", function() { return ERROR_CODE_CREATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERROR_CODE_STORE", function() { return ERROR_CODE_STORE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERROR_CODE_ATTACH", function() { return ERROR_CODE_ATTACH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAILURE_CLIENT", function() { return FAILURE_CLIENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAILURE_SERVER", function() { return FAILURE_SERVER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAILURE_CONNECTIVITY", function() { return FAILURE_CONNECTIVITY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FileUploadError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorFromDirectUploadMessage", function() { return errorFromDirectUploadMessage; });
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.includes */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.map */ "./node_modules/core-js/modules/es.map.js");
/* harmony import */ var core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_map__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.object.get-prototype-of */ "./node_modules/core-js/modules/es.object.get-prototype-of.js");
/* harmony import */ var core_js_modules_es_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.object.set-prototype-of */ "./node_modules/core-js/modules/es.object.set-prototype-of.js");
/* harmony import */ var core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.reflect.construct */ "./node_modules/core-js/modules/es.reflect.construct.js");
/* harmony import */ var core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_reflect_construct__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.includes */ "./node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__);
















function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// Error while reading the file client-side
var ERROR_CODE_READ = 'file-upload-read-error'; // Error while creating the empty blob on the server

var ERROR_CODE_CREATE = 'file-upload-create-error'; // Error while uploading the blob content

var ERROR_CODE_STORE = 'file-upload-store-error'; // Error while attaching the blob to the record

var ERROR_CODE_ATTACH = 'file-upload-attach-error'; // Failure on the client side (syntax error, error reading file, etc.)

var FAILURE_CLIENT = 'file-upload-failure-client'; // Failure on the server side (typically non-200 response)

var FAILURE_SERVER = 'file-upload-failure-server'; // Failure during the transfert (request aborted, connection lost, etc)

var FAILURE_CONNECTIVITY = 'file-upload-failure-connectivity';
/**
  Represent an error during a file upload.
  */

var FileUploadError = /*#__PURE__*/function (_Error) {
  _inherits(FileUploadError, _Error);

  var _super = _createSuper(FileUploadError);

  function FileUploadError(message, status, code) {
    var _this;

    _classCallCheck(this, FileUploadError);

    _this = _super.call(this, message);
    _this.name = 'FileUploadError';
    _this.status = status;
    _this.code = code; // Prevent the constructor stacktrace from being included.
    // (it messes up with Sentry issues grouping)

    if (Error.captureStackTrace) {
      // V8-only
      Error.captureStackTrace(_assertThisInitialized(_this), _this.constructor);
    } else {
      _this.stack = new Error().stack;
    }

    return _this;
  }
  /**
    Return the component responsible of the error (client, server or connectivity).
    See FAILURE_* constants for values.
    */


  _createClass(FileUploadError, [{
    key: "failureReason",
    get: function get() {
      var isNetworkError = this.code && this.code != ERROR_CODE_READ;

      if (isNetworkError && this.status != 0) {
        return FAILURE_SERVER;
      } else if (isNetworkError && this.status == 0) {
        return FAILURE_CONNECTIVITY;
      } else {
        return FAILURE_CLIENT;
      }
    }
  }]);

  return FileUploadError;
}( /*#__PURE__*/_wrapNativeSuper(Error)); // Convert an error message returned by DirectUpload to a proper error object.
//
// This function has two goals:
// 1. Remove the file name from the DirectUpload error message
//   (because the filename confuses Sentry error grouping)
// 2. Create each kind of error on a different line
//   (so that Sentry knows they are different kind of errors, from
//   the line they were created.)



function errorFromDirectUploadMessage(message) {
  var matches = message.match(/ Status: ([0-9]{1,3})/);
  var status = matches ? parseInt(matches[1], 10) : undefined; // prettier-ignore

  if (message.includes('Error reading')) {
    return new FileUploadError('Error reading file.', status, ERROR_CODE_READ);
  } else if (message.includes('Error creating')) {
    return new FileUploadError('Error creating file.', status, ERROR_CODE_CREATE);
  } else if (message.includes('Error storing')) {
    return new FileUploadError('Error storing file.', status, ERROR_CODE_STORE);
  } else {
    return new FileUploadError(message, status, undefined);
  }
}

/***/ }),

/***/ "./app/javascript/shared/activestorage/progress-bar.js":
/*!*************************************************************!*\
  !*** ./app/javascript/shared/activestorage/progress-bar.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ProgressBar; });
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_11__);













function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var PENDING_CLASS = 'direct-upload--pending';
var ERROR_CLASS = 'direct-upload--error';
var COMPLETE_CLASS = 'direct-upload--complete';
/**
  ProgressBar is and utility class responsible for
  rendering upload progress bar. It is used to handle
  direct-upload form ujs events but also in the
  Uploader delegate used with uploads on json api.

  As the associated DOM element may disappear for some
  reason (a dynamic React list, an element being removed
  and recreated again later, etc.), this class doesn't
  raise any error if the associated DOM element cannot
  be found.
  */

var ProgressBar = /*#__PURE__*/function () {
  _createClass(ProgressBar, null, [{
    key: "init",
    value: function init(input, id, file) {
      clearErrors(input);
      var html = this.render(id, file.name);
      input.insertAdjacentHTML('beforebegin', html);
    }
  }, {
    key: "start",
    value: function start(id) {
      var element = getDirectUploadElement(id);

      if (element) {
        element.classList.remove(PENDING_CLASS);
      }
    }
  }, {
    key: "progress",
    value: function progress(id, _progress) {
      var element = getDirectUploadProgressElement(id);

      if (element) {
        element.style.width = "".concat(_progress, "%");
      }
    }
  }, {
    key: "error",
    value: function error(id, _error) {
      var element = getDirectUploadElement(id);

      if (element) {
        element.classList.add(ERROR_CLASS);
        element.setAttribute('title', _error);
      }
    }
  }, {
    key: "end",
    value: function end(id) {
      var element = getDirectUploadElement(id);

      if (element) {
        element.classList.add(COMPLETE_CLASS);
      }
    }
  }, {
    key: "render",
    value: function render(id, filename) {
      return "<div id=\"direct-upload-".concat(id, "\" class=\"direct-upload ").concat(PENDING_CLASS, "\" data-direct-upload-id=\"").concat(id, "\">\n      <div class=\"direct-upload__progress\" style=\"width: 0%\"></div>\n      <span class=\"direct-upload__filename\">").concat(filename, "</span>\n    </div>");
    }
  }]);

  function ProgressBar(input, id, file) {
    _classCallCheck(this, ProgressBar);

    this.constructor.init(input, id, file);
    this.id = id;
  }

  _createClass(ProgressBar, [{
    key: "start",
    value: function start() {
      this.constructor.start(this.id);
    }
  }, {
    key: "progress",
    value: function progress(_progress2) {
      this.constructor.progress(this.id, _progress2);
    }
  }, {
    key: "error",
    value: function error(_error2) {
      this.constructor.error(this.id, _error2);
    }
  }, {
    key: "end",
    value: function end() {
      this.constructor.end(this.id);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var element = getDirectUploadElement(this.id);
      element.remove();
    }
  }]);

  return ProgressBar;
}();



function clearErrors(input) {
  var errorElements = input.parentElement.querySelectorAll(".".concat(ERROR_CLASS));

  var _iterator = _createForOfIteratorHelper(errorElements),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var element = _step.value;
      element.remove();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
}

function getDirectUploadElement(id) {
  return document.getElementById("direct-upload-".concat(id));
}

function getDirectUploadProgressElement(id) {
  return document.querySelector("#direct-upload-".concat(id, " .direct-upload__progress"));
}

/***/ }),

/***/ "./app/javascript/shared/activestorage/ujs.js":
/*!****************************************************!*\
  !*** ./app/javascript/shared/activestorage/ujs.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _progress_bar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-bar */ "./app/javascript/shared/activestorage/progress-bar.js");
/* harmony import */ var _file_upload_error__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./file-upload-error */ "./app/javascript/shared/activestorage/file-upload-error.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");



var INITIALIZE_EVENT = 'direct-upload:initialize';
var START_EVENT = 'direct-upload:start';
var PROGRESS_EVENT = 'direct-upload:progress';
var ERROR_EVENT = 'direct-upload:error';
var END_EVENT = 'direct-upload:end';

function addUploadEventListener(type, handler) {
  addEventListener(type, function (event) {
    // Internet Explorer and Edge will sometime replay Javascript events
    // that were dispatched just before a page navigation (!), but without
    // the event payload.
    //
    // Ignore these replayed events.
    var isEventValid = event && event.detail && event.detail.id != undefined;
    if (!isEventValid) return;
    handler(event);
  });
}

addUploadEventListener(INITIALIZE_EVENT, function (_ref) {
  var target = _ref.target,
      _ref$detail = _ref.detail,
      id = _ref$detail.id,
      file = _ref$detail.file;
  _progress_bar__WEBPACK_IMPORTED_MODULE_0__["default"].init(target, id, file);
});
addUploadEventListener(START_EVENT, function (_ref2) {
  var target = _ref2.target,
      id = _ref2.detail.id;
  _progress_bar__WEBPACK_IMPORTED_MODULE_0__["default"].start(id); // At the end of the upload, the form will be submitted again.
  // Avoid the confirm dialog to be presented again then.

  var button = target.form.querySelector('button.primary');

  if (button) {
    button.removeAttribute('data-confirm');
  }
});
addUploadEventListener(PROGRESS_EVENT, function (_ref3) {
  var _ref3$detail = _ref3.detail,
      id = _ref3$detail.id,
      progress = _ref3$detail.progress;
  _progress_bar__WEBPACK_IMPORTED_MODULE_0__["default"].progress(id, progress);
});
addUploadEventListener(ERROR_EVENT, function (event) {
  var id = event.detail.id;
  var errorMsg = event.detail.error; // Display an error message

  alert("Nous sommes d\xE9sol\xE9s, une erreur s\u2019est produite lors de l\u2019envoi du fichier.\n\n    (".concat(errorMsg, ")")); // Prevent ActiveStorage from displaying its own error message

  event.preventDefault();
  _progress_bar__WEBPACK_IMPORTED_MODULE_0__["default"].error(id, errorMsg); // Report unexpected client errors to Sentry.
  // (But ignore usual client errors, or errors we can monitor better on the server side.)

  var error = Object(_file_upload_error__WEBPACK_IMPORTED_MODULE_1__["errorFromDirectUploadMessage"])(errorMsg);

  if (error.failureReason == _file_upload_error__WEBPACK_IMPORTED_MODULE_1__["FAILURE_CLIENT"] && error.code != _file_upload_error__WEBPACK_IMPORTED_MODULE_1__["ERROR_CODE_READ"]) {
    Object(_utils__WEBPACK_IMPORTED_MODULE_2__["fire"])(document, 'sentry:capture-exception', error);
  }
});
addUploadEventListener(END_EVENT, function (_ref4) {
  var id = _ref4.detail.id;
  _progress_bar__WEBPACK_IMPORTED_MODULE_0__["default"].end(id);
});

/***/ }),

/***/ "./app/javascript/shared/franceconnect.js":
/*!************************************************!*\
  !*** ./app/javascript/shared/franceconnect.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_0__);

var fconnect = {
  tracesUrl: '/traces',
  aboutUrl: ''
};
var document = window.document;

function init() {
  fconnect.currentHost = 'fcp.integ01.dev-franceconnect.fr';
  if (window.location.hostname == 'www.demarches-simplifiees.fr') fconnect.currentHost = 'app.franceconnect.gouv.fr';
  var fconnectProfile = document.getElementById('fconnect-profile');

  if (fconnectProfile) {
    var linkAccess = document.querySelector('#fconnect-profile > a');
    var fcLogoutUrl = fconnectProfile.getAttribute('data-fc-logout-url');
    var access = createFCAccessElement(fcLogoutUrl);
    fconnectProfile.appendChild(access);
    linkAccess.onclick = toggleElement.bind(access);
  }
}

addEventListener('ds:page:update', init);

function toggleElement(event) {
  event.preventDefault();

  if (this.style.display === 'block') {
    this.style.display = 'none';
  } else {
    this.style.display = 'block';
  }
}

function closeFCPopin(event) {
  event.preventDefault();
  fconnect.popin.className = 'fade-out';
  setTimeout(function () {
    document.body.removeChild(fconnect.popin);
  }, 200);
}

function openFCPopin() {
  fconnect.popin = document.createElement('div');
  fconnect.popin.id = 'fc-background';
  var iframe = createFCIframe();
  document.body.appendChild(fconnect.popin);
  fconnect.popin.appendChild(iframe);
  setTimeout(function () {
    fconnect.popin.className = 'fade-in';
  }, 200);
}

function createFCIframe() {
  var iframe = document.createElement('iframe');
  iframe.setAttribute('id', 'fconnect-iframe');
  iframe.frameBorder = 0;
  iframe.name = 'fconnect-iframe';
  return iframe;
}

function createFCAccessElement(logoutUrl) {
  var access = document.createElement('div');
  access.id = 'fconnect-access';
  access.innerHTML = '<h5>Vous êtes identifié grâce à FranceConnect</h5>';
  access.appendChild(createAboutLink());
  access.appendChild(document.createElement('hr'));
  access.appendChild(createHistoryLink());
  access.appendChild(createLogoutElement(logoutUrl));
  return access;
}

function createHistoryLink() {
  var historyLink = document.createElement('a');
  historyLink.target = 'fconnect-iframe';
  historyLink.href = '//' + fconnect.currentHost + fconnect.tracesUrl;
  historyLink.onclick = openFCPopin;
  historyLink.innerHTML = 'Historique des connexions/échanges de données';
  return historyLink;
}

function createAboutLink() {
  var aboutLink = document.createElement('a');
  aboutLink.href = fconnect.aboutUrl ? '//' + fconnect.currentHost + fconnect.aboutUrl : '#';

  if (fconnect.aboutUrl) {
    aboutLink.target = 'fconnect-iframe';
    aboutLink.onclick = openFCPopin;
  }

  aboutLink.innerHTML = "Qu'est-ce-que FranceConnect ?";
  return aboutLink;
}

function createLogoutElement(logoutUrl) {
  var elm = document.createElement('div');
  elm.className = 'logout';
  elm.innerHTML = '<a class="btn btn-default" href="' + logoutUrl + '">Se déconnecter</a>';
  return elm;
}

var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
var eventer = window[eventMethod];
var messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message'; // Listen to message from child window

eventer(messageEvent, function (e) {
  var key = e.message ? 'message' : 'data';
  var data = e[key];

  if (data === 'close_popup') {
    closeFCPopin(e);
  }
}, false);

/***/ }),

/***/ "./app/javascript/shared/page-update-event.js":
/*!****************************************************!*\
  !*** ./app/javascript/shared/page-update-event.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");

addEventListener('DOMContentLoaded', function () {
  Object(_utils__WEBPACK_IMPORTED_MODULE_0__["fire"])(document, 'ds:page:update');
});
addEventListener('ajax:success', function () {
  Object(_utils__WEBPACK_IMPORTED_MODULE_0__["fire"])(document, 'ds:page:update');
});

/***/ }),

/***/ "./app/javascript/shared/polyfills.js":
/*!********************************************!*\
  !*** ./app/javascript/shared/polyfills.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_stable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/stable */ "./node_modules/core-js/stable/index.js");
/* harmony import */ var core_js_stable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_stable__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dom4__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dom4 */ "./node_modules/dom4/build/dom4.max.js");
/* harmony import */ var dom4__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dom4__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! intersection-observer */ "./node_modules/intersection-observer/intersection-observer.js");
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(intersection_observer__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _polyfills_insertAdjacentElement__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./polyfills/insertAdjacentElement */ "./app/javascript/shared/polyfills/insertAdjacentElement.js");
/* harmony import */ var _polyfills_insertAdjacentElement__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_polyfills_insertAdjacentElement__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _polyfills_dataset__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./polyfills/dataset */ "./app/javascript/shared/polyfills/dataset.js");
// Include runtime-polyfills for older browsers.
// Due to babel.config.js's 'useBuiltIns', only polyfills actually
// required by the browsers we support will be included.







/***/ }),

/***/ "./app/javascript/shared/polyfills/dataset.js":
/*!****************************************************!*\
  !*** ./app/javascript/shared/polyfills/dataset.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_13__);















function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/*
  @preserve dataset polyfill for IE < 11. See https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dataset and http://caniuse.com/#search=dataset

  @author ShirtlessKirk copyright 2015
  @license WTFPL (http://www.wtfpl.net/txt/copying)
*/
var dash = /-([a-z])/gi;
var dataRegEx = /^data-(.+)/;
var hasEventListener = !!document.addEventListener;
var test = document.createElement('_');
var DOMAttrModified = 'DOMAttrModified';
var mutationSupport = false;

function clearDataset(event) {
  delete event.target._datasetCache;
}

function toCamelCase(string) {
  return string.replace(dash, function (_, letter) {
    return letter.toUpperCase();
  });
}

function getDataset() {
  var dataset = {};

  var _iterator = _createForOfIteratorHelper(this.attributes),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var attribute = _step.value;
      var match = attribute.name.match(dataRegEx);

      if (match) {
        dataset[toCamelCase(match[1])] = attribute.value;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return dataset;
}

function mutation() {
  if (hasEventListener) {
    test.removeEventListener(DOMAttrModified, mutation, false);
  } else {
    test.detachEvent("on".concat(DOMAttrModified), mutation);
  }

  mutationSupport = true;
}

if (!test.dataset) {
  if (hasEventListener) {
    test.addEventListener(DOMAttrModified, mutation, false);
  } else {
    test.attachEvent("on".concat(DOMAttrModified), mutation);
  } // trigger event (if supported)


  test.setAttribute('foo', 'bar');
  Object.defineProperty(Element.prototype, 'dataset', {
    get: mutationSupport ? function get() {
      if (!this._datasetCache) {
        this._datasetCache = getDataset.call(this);
      }

      return this._datasetCache;
    } : getDataset
  });

  if (mutationSupport && hasEventListener) {
    // < IE9 supports neither
    document.addEventListener(DOMAttrModified, clearDataset, false);
  }
}

/***/ }),

/***/ "./app/javascript/shared/polyfills/insertAdjacentElement.js":
/*!******************************************************************!*\
  !*** ./app/javascript/shared/polyfills/insertAdjacentElement.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
  Updated w/ insertAdjacentElement
  @author Dan Levy @justsml
  2016-06-23

  Credit: @lyleunderwood - afterend patch/fix

  2011-10-10

  By Eli Grey, http://eligrey.com
  Public Domain.
  NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
*/
function insertAdjacentElement(position, elem) {
  var _this = this;

  var parent = this.parentNode;
  var node, first;

  switch (position.toLowerCase()) {
    case 'beforebegin':
      while (node = elem.firstChild) {
        parent.insertBefore(node, _this);
      }

      break;

    case 'afterbegin':
      first = _this.firstChild;

      while (node = elem.lastChild) {
        first = _this.insertBefore(node, first);
      }

      break;

    case 'beforeend':
      while (node = elem.firstChild) {
        _this.appendChild(node);
      }

      break;

    case 'afterend':
      parent.insertBefore(elem, _this.nextSibling);
      break;
  }

  return elem;
} // Method missing in Firefox < 48


if (!HTMLElement.prototype.insertAdjacentElement) {
  HTMLElement.prototype.insertAdjacentElement = insertAdjacentElement;
}

/***/ }),

/***/ "./app/javascript/shared/remote-input.js":
/*!***********************************************!*\
  !*** ./app/javascript/shared/remote-input.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @utils */ "./app/javascript/shared/utils.js");

var remote = 'data-remote';
var inputChangeSelector = "input[".concat(remote, "], textarea[").concat(remote, "]"); // This is a patch for ujs remote handler. Its purpose is to add
// a debounced input listener.

function handleRemote(event) {
  var element = this;

  if (isRemote(element)) {
    Object(_utils__WEBPACK_IMPORTED_MODULE_0__["fire"])(element, 'change', event);
  }
}

function isRemote(element) {
  var value = element.getAttribute(remote);
  return value && value !== 'false';
}

Object(_utils__WEBPACK_IMPORTED_MODULE_0__["delegate"])('input', inputChangeSelector, Object(_utils__WEBPACK_IMPORTED_MODULE_0__["debounce"])(handleRemote, 200));

/***/ }),

/***/ "./app/javascript/shared/safari-11-file-xhr-workaround.js":
/*!****************************************************************!*\
  !*** ./app/javascript/shared/safari-11-file-xhr-workaround.js ***!
  \****************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__);


// iOS 11.3 Safari / macOS Safari 11.1 empty <input type="file"> XHR bug workaround.
// This should work with every modern browser which supports ES5 (including IE9).
// https://stackoverflow.com/questions/49614091/safari-11-1-ajax-xhr-form-submission-fails-when-inputtype-file-is-empty
// https://github.com/rails/rails/issues/32440
document.addEventListener('ajax:before', function (e) {
  var inputs = e.target.querySelectorAll('input[type="file"]:not([disabled])');
  inputs.forEach(function (input) {
    if (input.files.length > 0) {
      return;
    }

    input.setAttribute('data-safari-temp-disabled', 'true');
    input.setAttribute('disabled', '');
  });
}); // You should call this by yourself when you aborted an ajax request by stopping a event in ajax:before hook.

document.addEventListener('ajax:beforeSend', function (e) {
  var inputs = e.target.querySelectorAll('input[type="file"][data-safari-temp-disabled]');
  inputs.forEach(function (input) {
    input.removeAttribute('data-safari-temp-disabled');
    input.removeAttribute('disabled');
  });
});

/***/ }),

/***/ "./app/javascript/shared/utils.js":
/*!****************************************!*\
  !*** ./app/javascript/shared/utils.js ***!
  \****************************************/
/*! exports provided: debounce, fire, csrfToken, show, hide, toggle, enable, disable, hasClass, addClass, removeClass, delegate, ajax, getJSON, scrollTo, scrollToBottom, on, isNumeric, timeoutable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fire", function() { return fire; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "csrfToken", function() { return csrfToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show", function() { return show; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hide", function() { return hide; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggle", function() { return toggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enable", function() { return enable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "disable", function() { return disable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasClass", function() { return hasClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addClass", function() { return addClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeClass", function() { return removeClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delegate", function() { return delegate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return ajax; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getJSON", function() { return getJSON; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "scrollTo", function() { return scrollTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "scrollToBottom", function() { return scrollToBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "on", function() { return on; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumeric", function() { return isNumeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timeoutable", function() { return timeoutable; });
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.from */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.join */ "./node_modules/core-js/modules/es.array.join.js");
/* harmony import */ var core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_reduce__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.reduce */ "./node_modules/core-js/modules/es.array.reduce.js");
/* harmony import */ var core_js_modules_es_array_reduce__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_reduce__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.object.keys */ "./node_modules/core-js/modules/es.object.keys.js");
/* harmony import */ var core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_keys__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @rails/ujs */ "./node_modules/@rails/ujs/lib/assets/compiled/rails-ujs.js");
/* harmony import */ var _rails_ujs__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(_rails_ujs__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var debounce__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! debounce */ "./node_modules/debounce/index.js");
/* harmony import */ var debounce__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(debounce__WEBPACK_IMPORTED_MODULE_23__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce__WEBPACK_IMPORTED_MODULE_23___default.a; });























function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var fire = _rails_ujs__WEBPACK_IMPORTED_MODULE_22___default.a.fire,
    csrfToken = _rails_ujs__WEBPACK_IMPORTED_MODULE_22___default.a.csrfToken;

function show(el) {
  el && el.classList.remove('hidden');
}
function hide(el) {
  el && el.classList.add('hidden');
}
function toggle(el, force) {
  if (force == undefined) {
    el & el.classList.toggle('hidden');
  } else if (force) {
    el && el.classList.remove('hidden');
  } else {
    el && el.classList.add('hidden');
  }
}
function enable(el) {
  el && (el.disabled = false);
}
function disable(el) {
  el && (el.disabled = true);
}
function hasClass(el, cssClass) {
  return el && el.classList.contains(cssClass);
}
function addClass(el, cssClass) {
  el && el.classList.add(cssClass);
}
function removeClass(el, cssClass) {
  el && el.classList.remove(cssClass);
}
function delegate(eventNames, selector, callback) {
  eventNames.split(' ').forEach(function (eventName) {
    return _rails_ujs__WEBPACK_IMPORTED_MODULE_22___default.a.delegate(document, selector, eventName, callback);
  });
}
function ajax(options) {
  return new Promise(function (resolve, reject) {
    Object.assign(options, {
      success: function success(response, statusText, xhr) {
        resolve({
          response: response,
          statusText: statusText,
          xhr: xhr
        });
      },
      error: function error(response, statusText, xhr) {
        var error = new Error("Erreur ".concat(xhr.status, " : ").concat(statusText));
        Object.assign(error, {
          response: response,
          statusText: statusText,
          xhr: xhr
        });
        reject(error);
      }
    });
    _rails_ujs__WEBPACK_IMPORTED_MODULE_22___default.a.ajax(options);
  });
}
function getJSON(url, data) {
  var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'GET';

  var _fetchOptions = fetchOptions(data, method),
      query = _fetchOptions.query,
      options = _objectWithoutProperties(_fetchOptions, ["query"]);

  return fetch("".concat(url).concat(query), options).then(function (response) {
    if (response.ok) {
      if (response.status === 204) {
        return null;
      }

      return response.json();
    }

    var error = new Error(response.statusText || response.status);
    error.response = response;
    throw error;
  });
}
function scrollTo(container, scrollTo) {
  container.scrollTop = offset(scrollTo).top - offset(container).top + container.scrollTop;
}
function scrollToBottom(container) {
  container.scrollTop = container.scrollHeight;
}
function on(selector, eventName, fn) {
  _toConsumableArray(document.querySelectorAll(selector)).forEach(function (element) {
    return element.addEventListener(eventName, function (event) {
      return fn(event, event.detail);
    });
  });
}
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function offset(element) {
  var rect = element.getBoundingClientRect();
  return {
    top: rect.top + document.body.scrollTop,
    left: rect.left + document.body.scrollLeft
  };
} // Takes a promise, and return a promise that times out after the given delay.


function timeoutable(promise, timeoutDelay) {
  var timeoutPromise = new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(new Error("Promise timed out after ".concat(timeoutDelay, "ms")));
    }, timeoutDelay);
  });
  return Promise.race([promise, timeoutPromise]);
}
var FETCH_TIMEOUT = 30 * 1000; // 30 sec

function fetchOptions(data) {
  var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
  var options = {
    query: '',
    method: method.toUpperCase(),
    headers: {
      accept: 'application/json',
      'x-csrf-token': csrfToken(),
      'x-requested-with': 'XMLHttpRequest'
    },
    credentials: 'same-origin'
  };

  if (data) {
    if (options.method === 'GET') {
      options.query = objectToQuerystring(data);
    } else {
      options.headers['content-type'] = 'application/json';
      options.body = JSON.stringify(data);
    }
  }

  if (window.AbortController) {
    var controller = new AbortController();
    options.signal = controller.signal;
    setTimeout(function () {
      controller.abort();
    }, FETCH_TIMEOUT);
  }

  return options;
}

function objectToQuerystring(obj) {
  return Object.keys(obj).reduce(function (query, key, i) {
    return [query, i === 0 ? '?' : '&', encodeURIComponent(key), '=', encodeURIComponent(obj[key])].join('');
  }, '');
}

/***/ })

}]);
//# sourceMappingURL=4-812a5f26e1919fb1ed96.chunk.js.map